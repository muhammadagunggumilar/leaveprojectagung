package com.bootcamp.leave.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bootcamp.leave.models.Position;

@Repository
public interface PositionRepository extends JpaRepository<Position, Long>{

}

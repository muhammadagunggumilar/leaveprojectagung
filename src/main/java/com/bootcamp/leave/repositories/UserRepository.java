package com.bootcamp.leave.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bootcamp.leave.models.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

}

package com.bootcamp.leave.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bootcamp.leave.models.PositionLeave;

@Repository
public interface PositionLeaveRepository extends JpaRepository<PositionLeave, Long>{

}

package com.bootcamp.leave.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bootcamp.leave.models.BucketApproval;

@Repository
public interface BucketApprovalRepository extends JpaRepository<BucketApproval, Long>{
	@Query(value = "SELECT * FROM bucket_approval WHERE user_leave_request_id IN (SELECT user_leave_request_id FROM user_leave_request WHERE user_id =:userId)",
			nativeQuery = true)
	Page<BucketApproval> findAllUser(@Param("userId") Long userId, Pageable pageable);
}

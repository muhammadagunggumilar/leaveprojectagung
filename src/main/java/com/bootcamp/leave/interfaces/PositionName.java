package com.bootcamp.leave.interfaces;

public interface PositionName {
	String EMPLOYEE = "Employee";
	String SUPERVISOR = "Supervisor";
	String STAFF = "Staff";
}

package com.bootcamp.leave.dtomodels;

import java.util.Date;

public class PositionDTO {
	private Long positionId;
	private String positionName;
	private String createdBy;
	private Date createdDate;
	private String updateBy;
	private Date updateDate;
	
	public PositionDTO() {}
	
	public PositionDTO(Long positionId, String positionName, String createdBy, Date createdDate, String updateBy,
			Date updateDate) {
		super();
		this.positionId = positionId;
		this.positionName = positionName;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updateBy = updateBy;
		this.updateDate = updateDate;
	}
	
	public Long getPositionId() {
		return positionId;
	}
	public void setPositionId(Long positionId) {
		this.positionId = positionId;
	}
	public String getPositionName() {
		return positionName;
	}
	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
}

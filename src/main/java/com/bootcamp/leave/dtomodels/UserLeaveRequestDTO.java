package com.bootcamp.leave.dtomodels;

import java.util.Date;

public class UserLeaveRequestDTO {
	private Long userLeaveRequestId;
	private UserDTO user;
	private String status;
	private Date leaveDate;
	private Date leaveDateFrom;
	private Date leaveDateTo;
	private int leaveLeft;
	private String description;
	private String createdBy;
	private Date createdDate;
	private String updateBy;
	private Date updateDate;
	
	public UserLeaveRequestDTO() {}
	
	public UserLeaveRequestDTO(Long userLeaveRequestId, UserDTO user, String status, Date leaveDate, Date leaveDateFrom,
			Date leaveDateTo, int leaveLeft, String description, String createdBy, Date createdDate, String updateBy,
			Date updateDate) {
		super();
		this.userLeaveRequestId = userLeaveRequestId;
		this.user = user;
		this.status = status;
		this.leaveDate = leaveDate;
		this.leaveDateFrom = leaveDateFrom;
		this.leaveDateTo = leaveDateTo;
		this.leaveLeft = leaveLeft;
		this.description = description;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updateBy = updateBy;
		this.updateDate = updateDate;
	}
	public Long getUserLeaveRequestId() {
		return userLeaveRequestId;
	}
	public void setUserLeaveRequestId(Long userLeaveRequestId) {
		this.userLeaveRequestId = userLeaveRequestId;
	}
	public UserDTO getUser() {
		return user;
	}
	public void setUser(UserDTO user) {
		this.user = user;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getLeaveDate() {
		return leaveDate;
	}
	public void setLeaveDate(Date leaveDate) {
		this.leaveDate = leaveDate;
	}
	public Date getLeaveDateFrom() {
		return leaveDateFrom;
	}
	public void setLeaveDateFrom(Date leaveDateFrom) {
		this.leaveDateFrom = leaveDateFrom;
	}
	public Date getLeaveDateTo() {
		return leaveDateTo;
	}
	public void setLeaveDateTo(Date leaveDateTo) {
		this.leaveDateTo = leaveDateTo;
	}
	public int getLeaveLeft() {
		return leaveLeft;
	}
	public void setLeaveLeft(int leaveLeft) {
		this.leaveLeft = leaveLeft;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
}

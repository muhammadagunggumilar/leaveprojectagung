package com.bootcamp.leave.dtomodels;

import java.util.Date;

public class BucketApprovalDTO {
	private Long bucketApprovalId;
	private UserLeaveRequestDTO userLeaveRequest;
	private String status;
	private UserDTO user;
	private String resolvedBy;
	private Date resolvedDate;
	private String resolvedReason;
	private String createdBy;
	private Date createdDate;
	private String updateBy;
	private Date updateDate;
	
	public BucketApprovalDTO() {}
	
	public BucketApprovalDTO(Long bucketApprovalId, UserLeaveRequestDTO userLeaveRequest, String status, UserDTO user,
			String resolvedBy, Date resolvedDate, String resolvedReason, String createdBy, Date createdDate,
			String updateBy, Date updateDate) {
		super();
		this.bucketApprovalId = bucketApprovalId;
		this.userLeaveRequest = userLeaveRequest;
		this.status = status;
		this.user = user;
		this.resolvedBy = resolvedBy;
		this.resolvedDate = resolvedDate;
		this.resolvedReason = resolvedReason;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updateBy = updateBy;
		this.updateDate = updateDate;
	}
	public Long getBucketApprovalId() {
		return bucketApprovalId;
	}
	public void setBucketApprovalId(Long bucketApprovalId) {
		this.bucketApprovalId = bucketApprovalId;
	}
	public UserLeaveRequestDTO getUserLeaveRequest() {
		return userLeaveRequest;
	}
	public void setUserLeaveRequest(UserLeaveRequestDTO userLeaveRequest) {
		this.userLeaveRequest = userLeaveRequest;
	}
	public UserDTO getUser() {
		return user;
	}
	public void setUser(UserDTO user) {
		this.user = user;
	}
	public String getResolvedBy() {
		return resolvedBy;
	}
	public void setResolvedBy(String resolvedBy) {
		this.resolvedBy = resolvedBy;
	}
	public Date getResolvedDate() {
		return resolvedDate;
	}
	public void setResolvedDate(Date resolvedDate) {
		this.resolvedDate = resolvedDate;
	}
	public String getResolvedReason() {
		return resolvedReason;
	}
	public void setResolvedReason(String resolvedReason) {
		this.resolvedReason = resolvedReason;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}

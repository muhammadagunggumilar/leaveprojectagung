package com.bootcamp.leave.dtomodels;

import java.util.Date;

public class PositionLeaveDTO {
	private Long positionLeaveId;
	private PositionDTO position;
	private int maxLeavePerYear;
	private String createdBy;
	private Date createdDate;
	private String updateBy;
	private Date updateDate;
	
	public PositionLeaveDTO() {}
	
	public PositionLeaveDTO(Long positionLeaveId, PositionDTO position, int maxLeavePerYear, String createdBy,
			Date createdDate, String updateBy, Date updateDate) {
		super();
		this.positionLeaveId = positionLeaveId;
		this.position = position;
		this.maxLeavePerYear = maxLeavePerYear;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updateBy = updateBy;
		this.updateDate = updateDate;
	}
	public Long getPositionLeaveId() {
		return positionLeaveId;
	}
	public void setPositionLeaveId(Long positionLeaveId) {
		this.positionLeaveId = positionLeaveId;
	}
	public PositionDTO getPosition() {
		return position;
	}
	public void setPosition(PositionDTO position) {
		this.position = position;
	}
	public int getMaxLeavePerYear() {
		return maxLeavePerYear;
	}
	public void setMaxLeavePerYear(int maxLeavePerYear) {
		this.maxLeavePerYear = maxLeavePerYear;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	
}

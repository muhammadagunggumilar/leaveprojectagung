package com.bootcamp.leave.dtomodels;

import java.util.Date;

public class UserDTO {
	private Long userId;
	private PositionDTO position;
	private String userName;
	private String createdBy;
	private Date createdDate;
	private String updateBy;
	private Date updateDate;
	
	public UserDTO() {}
	
	public UserDTO(Long userId, PositionDTO position, String userName, String createdBy, Date createdDate,
			String updateBy, Date updateDate) {
		super();
		this.userId = userId;
		this.position = position;
		this.userName = userName;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updateBy = updateBy;
		this.updateDate = updateDate;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public PositionDTO getPosition() {
		return position;
	}
	public void setPosition(PositionDTO position) {
		this.position = position;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
}

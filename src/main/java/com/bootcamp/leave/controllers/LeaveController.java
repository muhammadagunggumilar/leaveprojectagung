package com.bootcamp.leave.controllers;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bootcamp.leave.dtomodels.BucketApprovalDTO;
import com.bootcamp.leave.dtomodels.PositionLeaveDTO;
import com.bootcamp.leave.dtomodels.UserDTO;
import com.bootcamp.leave.dtomodels.UserLeaveRequestDTO;
import com.bootcamp.leave.interfaces.PositionName;
import com.bootcamp.leave.models.BucketApproval;
import com.bootcamp.leave.models.PositionLeave;
import com.bootcamp.leave.models.User;
import com.bootcamp.leave.models.UserLeaveRequest;
import com.bootcamp.leave.repositories.BucketApprovalRepository;
import com.bootcamp.leave.repositories.PositionLeaveRepository;
import com.bootcamp.leave.repositories.PositionRepository;
import com.bootcamp.leave.repositories.UserLeaveRequestRepository;
import com.bootcamp.leave.repositories.UserRepository;

@RestController
@RequestMapping("/api/leave")
public class LeaveController implements PositionName{
	@Autowired
	PositionRepository positionRepository;
	
	@Autowired
	PositionLeaveRepository positionLeaveRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	UserLeaveRequestRepository userLeaveRequestRepository;
	
	@Autowired
	BucketApprovalRepository bucketApprovalRepository;
		
	ModelMapper modelMapper = new ModelMapper();
	
	//no 2
	//leaveRequest
	@PostMapping("/requestLeave")
	public Map<String, Object> postRequestLeave(@Valid @RequestBody UserLeaveRequestDTO userLeaveRequestDTO){
		Map<String, Object> result = new HashMap<>();
				
		//mengextract date from
		Calendar calendarFrom = Calendar.getInstance();
		Date dateFrom = userLeaveRequestDTO.getLeaveDateFrom();
		calendarFrom.setTime(dateFrom);
		int yearFrom = calendarFrom.get(Calendar.YEAR);
		int monthFrom = calendarFrom.get(Calendar.MONTH);
		int dayFrom = calendarFrom.get(Calendar.DATE);

		//mengextract date to
		Calendar calendarTo = Calendar.getInstance();
		Date dateTo = userLeaveRequestDTO.getLeaveDateTo();
		calendarTo.setTime(dateTo);
		int yearTo = calendarTo.get(Calendar.YEAR);
		int monthTo = calendarTo.get(Calendar.MONTH);
		int dayTo = calendarTo.get(Calendar.DATE);
		
		//mengextract current date
		Calendar currentCalendar = Calendar.getInstance();
		int currentYear = currentCalendar.get(Calendar.YEAR);
		int currentMonth = currentCalendar.get(Calendar.MONTH);
		int currentDay = currentCalendar.get(Calendar.DATE);
				
		int maxLeavePerYear = foundMaxLeavePerYear(userLeaveRequestDTO, yearFrom);
		int minLeaveLeft = 0;
		Long diff = dateTo.getTime() - dateFrom.getTime();
		int range = (int) (diff / (24 * 60 * 60 * 1000));
				
		if(maxLeavePerYear <= minLeaveLeft){
			result.put("Status", 400);
			result.put("Status", "Mohon maaf, jatah cuti Anda telah habis.");
		}
		else if(range > maxLeavePerYear) {
			String message = "Mohon maaf, jatah cuti Anda tidak cukup untuk digunakan dari tanggal " + dayFrom+"/"+monthFrom+"/"+yearFrom+ " sampai " + dayTo+"/"+monthTo+"/"+yearTo+ " ("+ range + " hari). Jatah cuti Anda yang tersisa adalah "+maxLeavePerYear+" hari.";
			
			result.put("Status", 400);
			result.put("Message", message );
		}
		else if((yearFrom > yearTo) || (yearFrom == yearTo && monthFrom > monthTo) || (yearFrom == yearTo && monthFrom == monthTo && dayFrom >= dayTo)) {
			result.put("Status", 400);
			result.put("Message", "Tanggal yang Anda ajukan tidak valid.");
		}
		else if(yearFrom <= currentYear && monthFrom <= currentMonth && dayFrom <= currentDay) {
			result.put("Status", 400);
			result.put("Message", "Tanggal yang Anda ajukan telah lampau, silahkan ganti tanggal pengajuan cuti anda." );
		}
		else {
			List<UserLeaveRequest> listUserLeaveRequest = userLeaveRequestRepository.findAll();
			List<UserLeaveRequestDTO> listUserLeaveRequestDTO = new ArrayList<UserLeaveRequestDTO>();
			
			//mapping		
			for(UserLeaveRequest item : listUserLeaveRequest) {
				UserLeaveRequestDTO data = modelMapper.map(item, UserLeaveRequestDTO.class);
				listUserLeaveRequestDTO.add(data);
			}
			
			boolean isNotFound = true;
			for(UserLeaveRequestDTO item : listUserLeaveRequestDTO) {
				Long userId = item.getUser().getUserId();
				Long userIdDTO = userLeaveRequestDTO.getUser().getUserId();
				if(userId == userIdDTO) {
					//cari tanggal
					Date leaveDate = item.getLeaveDateFrom();
					Calendar leaveCalendar = Calendar.getInstance();
					leaveCalendar.setTime(leaveDate);
					int leaveYear = leaveCalendar.get(Calendar.YEAR);
					int leaveMonth = leaveCalendar.get(Calendar.MONTH);
					int leaveDay = leaveCalendar.get(Calendar.DATE);
					
					if(yearFrom == leaveYear && monthFrom == leaveMonth && leaveDay == dayFrom) {
						isNotFound = false;
					}
				}
			}
			
			if(isNotFound) {
				String status = "Waiting";
				UserLeaveRequest userLeaveRequest = modelMapper.map(userLeaveRequestDTO, UserLeaveRequest.class);
				userLeaveRequest.setLeaveLeft(maxLeavePerYear);
				userLeaveRequest.setStatus(status);
				BucketApproval bucketApproval = new BucketApproval();
				bucketApproval.setUserLeaveRequest(userLeaveRequestRepository.save(userLeaveRequest));
				bucketApproval.setStatus(status);
				bucketApprovalRepository.save(bucketApproval);
				
				result.put("Status", 200);
				result.put("Message", "Permohonan Anda sedang diproses." );
			}
			else{
				result.put("Status", 200);
				result.put("Message", "Permohonan Anda sudah ada dan sedang dalam proses." );
			}
		}
		
		return result;
	}
	
	//no 3
	//listRequestLeave/{userId}/{totalDataPerPage}/{choosenPage}
	@GetMapping("/listRequestLeave/{userId}/{totalDataPerPage}/{choosenPage}")
	public Map<String, Object> getListRequestLeave(@PathVariable(value="userId") Long userId,
			@PathVariable(value="totalDataPerPage") int totalDataPerPage, 
			@PathVariable(value="choosenPage") int choosenPage){
		Map<String, Object> result = new HashMap<String, Object>();
		Pageable pageable = PageRequest.of(choosenPage, totalDataPerPage);
		Page<BucketApproval> listBucketApproval = bucketApprovalRepository.findAllUser(userId, pageable);
		List<BucketApprovalDTO> listRequestLeave = new ArrayList<BucketApprovalDTO>();
		
		for(BucketApproval item : listBucketApproval) {
			BucketApprovalDTO data = modelMapper.map(item, BucketApprovalDTO.class);
			listRequestLeave.add(data);
		}
		
		result.put("Status", 200);
		result.put("Message", "Get List SUCCESSFULL" );
		result.put("Items", listRequestLeave);
		return result;
	}
	
	//no 4
	//API POST resolveRequestLeave
	@PostMapping("/resolveRequestLeave")
	public Map<String, Object> resolveRequestLeave(@Valid @RequestBody BucketApprovalDTO bucketApprovalDTO){
		Map<String, Object> result = new HashMap<>();
		long bucketApprovalId = bucketApprovalDTO.getBucketApprovalId();
		
		List<BucketApproval> listBucketApproval = bucketApprovalRepository.findAll();
		List<BucketApprovalDTO> listBucketApprovalDTO = new ArrayList<BucketApprovalDTO>();
		List<UserLeaveRequest> listUserLeaveRequest = userLeaveRequestRepository.findAll();
		List<UserLeaveRequestDTO> listUserLeaveRequestDTO = new ArrayList<UserLeaveRequestDTO>();
		List<User> listUser = userRepository.findAll();
		List<UserDTO> listUserDTO = new ArrayList<UserDTO>();
		
		//mapping user leave request
		for(UserLeaveRequest item : listUserLeaveRequest) {
			UserLeaveRequestDTO data = modelMapper.map(item, UserLeaveRequestDTO.class);
			listUserLeaveRequestDTO.add(data);
		}
		
		//mapping bucket approval
		for(BucketApproval item : listBucketApproval) {
			BucketApprovalDTO data = modelMapper.map(item, BucketApprovalDTO.class);
			listBucketApprovalDTO.add(data);
		}
		
		//mapping user
		for(User item : listUser) {
			UserDTO data = modelMapper.map(item, UserDTO.class);
			listUserDTO.add(data);
		}
		
		//cari id
		boolean isNotFound = true;
		for(BucketApprovalDTO item : listBucketApprovalDTO) {
			long id = item.getBucketApprovalId();
			if(id==bucketApprovalId) {
				isNotFound = false;
			}
		}
		
		if(isNotFound) {
			result.put("Status", 400);
			result.put("Message", "Permohonan dengan ID " +bucketApprovalId+ " tidak ditemukan.");
		}
		else{ 
			BucketApproval bucketApproval = bucketApprovalRepository.findById(bucketApprovalId).get();
			
			//cari date
			boolean dateNotFound = false;
			for(UserLeaveRequestDTO item : listUserLeaveRequestDTO) {
				long userId = item.getUser().getUserId();
				long userIdDTO = bucketApproval.getUserLeaveRequest().getUser().getUserId();
				
				if(userId == userIdDTO) {
					//extraact date dari item
					Date leaveDate = item.getLeaveDate();
					Calendar leaveCalendar = Calendar.getInstance();
					leaveCalendar.setTime(leaveDate);
					int leaveYear = leaveCalendar.get(Calendar.YEAR);
					int leaveMonth = leaveCalendar.get(Calendar.MONTH);
					int leaveDay = leaveCalendar.get(Calendar.DATE);
					
					//extract date dari dto
					Date resolvedDate = bucketApprovalDTO.getResolvedDate();
					Calendar resolvedCalendar = Calendar.getInstance();
					resolvedCalendar.setTime(resolvedDate);
					int resolvedYear = resolvedCalendar.get(Calendar.YEAR);
					int resolvedMonth = resolvedCalendar.get(Calendar.MONTH);
					int resolvedDay = resolvedCalendar.get(Calendar.DATE);
					
					if((leaveYear > resolvedYear) || (leaveYear == resolvedYear && leaveMonth > resolvedMonth) || (leaveYear == resolvedYear && leaveMonth == resolvedMonth && leaveDay > resolvedDay)) {
						dateNotFound = true;
					}
				}
			}
			
			if(dateNotFound) {
				result.put("Status", 400);
				result.put("Message", "Kesalahan data, tanggal keputusan tidak bisa lebih awal dari pengajuan cuti.");
			}
			else {
				long userApprovalId = bucketApprovalDTO.getUser().getUserId();
				String positionUserApproval = "";
				for(UserDTO item : listUserDTO) {
					long id = item.getUserId();
					if(id==userApprovalId) {
						positionUserApproval = item.getPosition().getPositionName();
					}
					
				}
				
				BucketApprovalDTO bucketApprovalDTO1 = modelMapper.map(bucketApproval, BucketApprovalDTO.class);
				
				String positionUserLeaveRequest = bucketApprovalDTO1.getUserLeaveRequest().getUser().getPosition().getPositionName();
				long userLeaveRequestId = bucketApprovalDTO1.getUserLeaveRequest().getUser().getUserId();
				
				boolean allowed = false;
				if(positionUserApproval.equalsIgnoreCase(SUPERVISOR) && positionUserLeaveRequest.equalsIgnoreCase(EMPLOYEE)) {
					allowed = true;
				}
				else if(positionUserApproval.equalsIgnoreCase(SUPERVISOR) && positionUserLeaveRequest.equalsIgnoreCase(SUPERVISOR)){
					if(userApprovalId == userLeaveRequestId) {
						allowed = true;
					}
				}
				else if(positionUserApproval.equalsIgnoreCase(STAFF) && positionUserLeaveRequest.equalsIgnoreCase(STAFF)) {
					allowed = true;
				}
				else if(positionUserApproval.equalsIgnoreCase(STAFF) && positionUserLeaveRequest.equalsIgnoreCase(SUPERVISOR)) {
					allowed = true;
				}
				else if(positionUserApproval.equalsIgnoreCase(STAFF) && positionUserLeaveRequest.equalsIgnoreCase(EMPLOYEE)) {
					allowed = true;
				}
				
				if(!allowed) {
					result.put("Status", 400);
					result.put("Message", "ACCESS DENIED (Posisi Anda tidak memenuhi syarat).");
				}
				else {
					long userLeaveRequestId1 = bucketApproval.getUserLeaveRequest().getUserLeaveRequestId();
					UserLeaveRequest userLeaveRequest = userLeaveRequestRepository.findById(userLeaveRequestId1).get();
					String status = bucketApprovalDTO.getStatus();
					userLeaveRequest.setStatus(status);
					//menghitung sisa cuti
					Date dateTo = userLeaveRequest.getLeaveDateTo();
					Date dateFrom = userLeaveRequest.getLeaveDateFrom();
					Long diff = dateTo.getTime() - dateFrom.getTime();
					int range = (int) (diff / (24 * 60 * 60 * 1000));
					userLeaveRequest.setLeaveLeft(range);
					Timestamp createdDate = (Timestamp) bucketApproval.getCreatedDate();
					
					bucketApproval = modelMapper.map(bucketApprovalDTO, BucketApproval.class);
					bucketApproval.setBucketApprovalId(bucketApprovalId);
					bucketApproval.setUserLeaveRequest(userLeaveRequest);
					bucketApproval.setCreatedDate(createdDate);
					
					bucketApprovalRepository.save(bucketApproval);
					
					result.put("Status", 200);
					result.put("Message", "Permohonan dengan ID " +bucketApprovalId+ " telah berhasil diputuskan.");
				}
			}
		}
		return result;
	}
	
	//cari max leave per year
	public int foundMaxLeavePerYear(UserLeaveRequestDTO userLeaveRequestDTO, int dateFrom) {
		int result = 0;
		
		List<PositionLeave> listPositionLeave = positionLeaveRepository.findAll();
		List<PositionLeaveDTO> listPositionLeaveDTO = new ArrayList<>();
		
		List<UserLeaveRequest> listUserLeaveRequest = userLeaveRequestRepository.findAll();
		List<UserLeaveRequestDTO> listUserLeaveRequestDTO = new ArrayList<UserLeaveRequestDTO>();
		
		List<User> listUser = userRepository.findAll();
		List<UserDTO> listUserDTO = new ArrayList<>();
		
		//mapping
		for(PositionLeave item : listPositionLeave) {
			PositionLeaveDTO data = modelMapper.map(item, PositionLeaveDTO.class);
			listPositionLeaveDTO.add(data);
		}
		
		for(UserLeaveRequest item : listUserLeaveRequest) {
			UserLeaveRequestDTO data = modelMapper.map(item, UserLeaveRequestDTO.class);
			listUserLeaveRequestDTO.add(data);
		}
		
		for(User item : listUser) {
			UserDTO data = modelMapper.map(item, UserDTO.class);
			listUserDTO.add(data);
		}
		
		//cari max leave per year
		boolean isNotFound = true;
		for(UserLeaveRequestDTO item : listUserLeaveRequestDTO) {
			Long positionId = item.getUser().getPosition().getPositionId();
			Long userIdDTO = userLeaveRequestDTO.getUser().getUserId();
			Long positionIdDTO = (long) 0;
			//cari psosisi id
			for(UserDTO data : listUserDTO) {
				Long id = data.getUserId();
				if(id==userIdDTO) {
					positionIdDTO = data.getPosition().getPositionId();
				}
			}
			Date leaveDate = item.getLeaveDateFrom();
			Calendar leaveCalendar = Calendar.getInstance();
			leaveCalendar.setTime(leaveDate);
			int leaveYear = leaveCalendar.get(Calendar.YEAR);
			if(positionIdDTO == positionId) {
				if(leaveYear == dateFrom) {
					result = item.getLeaveLeft();
					isNotFound = false;
				}
			}
		}
		
		if(isNotFound) {
			for(PositionLeaveDTO item : listPositionLeaveDTO) {
				Long positionId = item.getPosition().getPositionId();
				Long userIdDTO = userLeaveRequestDTO.getUser().getUserId();
				Long positionIdDTO = (long) 0;
				//cari psosisi id
				for(UserDTO data : listUserDTO) {
					Long id = data.getUserId();
					if(id==userIdDTO) {
						positionIdDTO = data.getPosition().getPositionId();
					}
				}
				if(positionIdDTO == positionId) {
					result = item.getMaxLeavePerYear();
				}
			}
		}
		
		return result;
	}
	
}

package com.bootcamp.leave.controllers;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bootcamp.leave.dtomodels.UserDTO;
import com.bootcamp.leave.models.User;
import com.bootcamp.leave.repositories.UserRepository;

@RestController
@RequestMapping("/api/user")
public class UserController {
	@Autowired
	UserRepository userRepository;

	ModelMapper modelMapper = new ModelMapper();
	
	//create
	@PostMapping("/create")
	public Map<String, Object> createUser(@Valid @RequestBody UserDTO userDTO){
		Map<String, Object> result = new HashMap<>();
		User user = modelMapper.map(userDTO, User.class);
		userRepository.save(user);

		result.put("Status", 200);
		result.put("Message", "Create User SUCCESSFULLY");
		return result;
	}
	
	//read
	@GetMapping("/read")
	public Map<String, Object> getUser(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<User> listUser = userRepository.findAll();
		List<UserDTO> listUserDTO = new ArrayList<>();
		
		for(User user : listUser) {
			UserDTO userDTO = modelMapper.map(user, UserDTO.class);
			listUserDTO.add(userDTO);
		}

		result.put("Status", 200);
		result.put("Message", "Read User SUCCESSFULL");
		result.put("Data", listUserDTO);
		
		return result;
	}
		
	//readby id
	@GetMapping("/read/{userId}")
	public Map<String, Object> getUserById(@PathVariable(value="userId") Long userId){
		Map<String, Object> result = new HashMap<String, Object>();
		User user = userRepository.findById(userId).get();
		UserDTO userDTO = modelMapper.map(user, UserDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Read User SUCCESSFULL");
		result.put("Data", userDTO);
		
		return result;
	}
		
	//update
	@PutMapping("/update/{userId}")
	public Map<String, Object> updateUser(@PathVariable(value="userId") Long userId,
			@Valid @RequestBody UserDTO userDTO){
		Map<String, Object> result = new HashMap<>();
		User user = userRepository.findById(userId).get();
		Timestamp date = (Timestamp) user.getCreatedDate();
		String createdBy = user.getCreatedBy();
		user = modelMapper.map(userDTO, User.class);
		user.setUserId(userId);
		user.setCreatedDate(date);
		user.setCreatedBy(createdBy);
		userRepository.save(user);
		
		result.put("Status", 200);
		result.put("Message", "Update User SUCCESSFULL");
		result.put("Data", userDTO);
		
		return result;
	}
	
	//delete
	@DeleteMapping("/delete/{userId}")
	public Map<String, Object> deleteUser(@PathVariable(value="userId") Long userId){
		Map<String, Object> result = new HashMap<String, Object>();
		User user = userRepository.findById(userId).get();
		userRepository.delete(user);

		result.put("Status", 200);
		result.put("Message", "Delete User SUCCESSFULL");
		
		return result;
	}
	
}

package com.bootcamp.leave.controllers;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bootcamp.leave.dtomodels.PositionLeaveDTO;
import com.bootcamp.leave.models.PositionLeave;
import com.bootcamp.leave.repositories.PositionLeaveRepository;

@RestController
@RequestMapping("/api/positionLeave")
public class PositionLeaveController {
	@Autowired
	PositionLeaveRepository positionLeaveRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	//create
	@PostMapping("/create")
	public Map<String, Object> createPositionLeave(@Valid @RequestBody PositionLeaveDTO positionLeaveDTO){
		Map<String, Object> result = new HashMap<>();
		PositionLeave positionLeave = modelMapper.map(positionLeaveDTO, PositionLeave.class);
		positionLeaveRepository.save(positionLeave);

		result.put("Status", 200);
		result.put("Message", "Create User SUCCESSFULLY");
		return result;
	}
	
	//read
	@GetMapping("/read")
	public Map<String, Object> getPositionLeave(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<PositionLeave> listPositionLeave = positionLeaveRepository.findAll();
		List<PositionLeaveDTO> listPositionLeaveDTO = new ArrayList<>();
		
		for(PositionLeave positionLeave : listPositionLeave) {
			PositionLeaveDTO positionLeaveDTO = modelMapper.map(positionLeave, PositionLeaveDTO.class);
			listPositionLeaveDTO.add(positionLeaveDTO);
		}

		result.put("Status", 200);
		result.put("Message", "Read Position Leave SUCCESSFULL");
		result.put("Data", listPositionLeaveDTO);
		
		return result;
	}
		
	//readby id
	@GetMapping("/read/{positionLeaveId}")
	public Map<String, Object> getPositionMapById(@PathVariable(value="positionLeaveId") Long positionLeaveId){
		Map<String, Object> result = new HashMap<String, Object>();
		PositionLeave positionLeave = positionLeaveRepository.findById(positionLeaveId).get();
		PositionLeaveDTO positionLeaveDTO = modelMapper.map(positionLeave, PositionLeaveDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Read Position LeaveSUCCESSFULL");
		result.put("Data", positionLeaveDTO);
		
		return result;
	}
		
	//update
	@PutMapping("/update/{positionLeaveId}")
	public Map<String, Object> updatePositionLeave(@PathVariable(value="positionLeaveId") Long positionLeaveId,
			@Valid @RequestBody PositionLeaveDTO positionLeaveDTO){
		Map<String, Object> result = new HashMap<>();
		PositionLeave positionLeave = positionLeaveRepository.findById(positionLeaveId).get();
		Timestamp date = (Timestamp) positionLeave.getCreatedDate();
		String createdBy = positionLeave.getCreatedBy();
		positionLeave = modelMapper.map(positionLeaveDTO, PositionLeave.class);
		positionLeave.setPositionLeaveId(positionLeaveId);
		positionLeave.setCreatedDate(date);
		positionLeave.setCreatedBy(createdBy);
		positionLeaveRepository.save(positionLeave);
		
		result.put("Status", 200);
		result.put("Message", "Update Position Leave SUCCESSFULL");
		result.put("Data", positionLeaveDTO);
		
		return result;
	}
		
	//delete
	@DeleteMapping("/delete/{positionLeaveId}")
	public Map<String, Object> deletePositionLeave(@PathVariable(value="positionLeaveId") Long positionLeaveId){
		Map<String, Object> result = new HashMap<String, Object>();
		PositionLeave positionLeave = positionLeaveRepository.findById(positionLeaveId).get();
		positionLeaveRepository.delete(positionLeave);

		result.put("Status", 200);
		result.put("Message", "Delete Position Leave SUCCESSFULL");
		
		return result;
	}
}

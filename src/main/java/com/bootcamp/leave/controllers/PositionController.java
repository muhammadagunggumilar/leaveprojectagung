package com.bootcamp.leave.controllers;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bootcamp.leave.dtomodels.PositionDTO;
import com.bootcamp.leave.models.Position;
import com.bootcamp.leave.repositories.PositionRepository;

@RestController
@RequestMapping("/api/position")
public class PositionController {
	@Autowired
	PositionRepository positionRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	//create
	@PostMapping("/create")
	public Map<String, Object> createPosition(@Valid @RequestBody PositionDTO positionDTO){
		Map<String, Object> result = new HashMap<>();
		Position position = modelMapper.map(positionDTO, Position.class);
		positionRepository.save(position);

		result.put("Status", 200);
		result.put("Message", "Create User SUCCESSFULLY");
		return result;
	}
	
	//read
	@GetMapping("/read")
	public Map<String, Object> getPosition(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<Position> listPosition = positionRepository.findAll();
		List<PositionDTO> listPositionDTO = new ArrayList<>();
		
		for(Position position : listPosition) {
			PositionDTO positionDTO = modelMapper.map(position, PositionDTO.class);
			listPositionDTO.add(positionDTO);
		}

		result.put("Status", 200);
		result.put("Message", "Read Position SUCCESSFULL");
		result.put("Data", listPositionDTO);
		
		return result;
	}
		
	//readby id
	@GetMapping("/read/{positionId}")
	public Map<String, Object> getPositionById(@PathVariable(value="positionId") Long positionId){
		Map<String, Object> result = new HashMap<String, Object>();
		Position position = positionRepository.findById(positionId).get();
		PositionDTO positionDTO = modelMapper.map(position, PositionDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Read Position SUCCESSFULL");
		result.put("Data", positionDTO);
		
		return result;
	}
		
	//update
	@PutMapping("/update/{positionId}")
	public Map<String, Object> updatePosition(@PathVariable(value="positionId") Long positionId,
			@Valid @RequestBody PositionDTO positionDTO){
		Map<String, Object> result = new HashMap<>();
		Position position = positionRepository.findById(positionId).get();
		Timestamp date = (Timestamp) position.getCreatedDate();
		String createdBy = position.getCreatedBy();
		position = modelMapper.map(positionDTO, Position.class);
		position.setPositionId(positionId);
		position.setCreatedDate(date);
		position.setCreatedBy(createdBy);
		positionRepository.save(position);
		
		result.put("Status", 200);
		result.put("Message", "Update Position SUCCESSFULL");
		result.put("Data", positionDTO);
		
		return result;
	}
	
	//delete
	@DeleteMapping("/delete/{positionId}")
	public Map<String, Object> deletePosition(@PathVariable(value="positionId") Long positionId){
		Map<String, Object> result = new HashMap<String, Object>();
		Position position = positionRepository.findById(positionId).get();
		positionRepository.delete(position);

		result.put("Status", 200);
		result.put("Message", "Delete Position SUCCESSFULL");
		
		return result;
	}
}
